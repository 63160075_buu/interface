/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.interfaceproject;

/**
 *
 * @author ACER
 */
public class Testcode {

    public static void main(String[] args) {
        Bat bat = new Bat("Katakuri");
        Bird bird = new Bird("Torao");
        Plane plane = new Plane();
        Croco croco = new Croco("Croc Jang");
        Sneak sneak = new Sneak("A poo");
        Fish fish = new Fish("Tong");
        Crab crab = new Crab("God Usop");
        Car car = new Car();
        Human human = new Human("Too");
        Cat cat = new Cat("Sanji");
        Dog dog = new Dog("Zoro");

        Run[] run = {human, dog, cat, car, plane};
        for (Run r : run) {
            if (r instanceof Land) {
                Land l = (Land) r;
                l.eat();
                l.walk();
                l.run();
                l.speak();
                l.sleep();
            } else {
                if (r instanceof Car) {
                    Car c = (Car) r;
                    c.startPower();
                    c.run();
                    c.stopPower();
                } else if (r instanceof Plane) {
                    Plane p = (Plane) r;
                    p.startPower();
                    p.run();
                    p.stopPower();
                }
            }
            
            System.out.println("--------------------------------");

            Swim[] swim = {fish, crab};
            for (Swim s : swim) {
                if (s instanceof Aquatic) {
                    Aquatic a = (Aquatic) s;
                    a.eat();
                    a.walk();
                    a.swim();
                    a.speak();
                    a.sleep();
                }
            }
        }
        
            System.out.println("--------------------------------");

            Craw[] crawl = {croco, sneak};
            for (Craw c : crawl) {
                if (c instanceof Reptile) {
                    Reptile r = (Reptile) c;
                    r.eat();
                    r.walk();
                    r.crawl();
                    r.speak();
                    r.sleep();
                }
            }
            
            System.out.println("--------------------------------");


            Fly[] fly = {bat, bird, plane};
            for (Fly f : fly) {
                if (f instanceof Peak) {
                    Peak p = (Peak) f;
                    p.eat();
                    p.walk();
                    p.fly();
                    p.speak();
                    p.sleep();
                } else if (f instanceof Plane) {
                    Plane p = (Plane) f;
                    p.startPower();
                    p.run();
                    p.stopPower();
                }

            }
        }
    }

