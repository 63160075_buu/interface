/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.interfaceproject;

/**
 *
 * @author ACER
 */
public class Car extends Vehicle implements Run {

    public Car() {
        super("Car");
    }

    @Override
    public void startPower() {
        System.out.println("Car: Start Power.");
    }

    @Override
    public void stopPower() {
        System.out.println("Car: Stop Power.");
    }

    @Override
    public void run() {
        System.out.println("Car: Run.");
    }
}
