/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.interfaceproject;

/**
 *
 * @author ACER
 */
public class Sneak extends Reptile {

    private String nickname;

    public Sneak(String nickname) {
        super("Sneak", 4);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Sneak: " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Sneak: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Sneak: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Sneak: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Sneak: " + nickname + " sleep");
    }
}
