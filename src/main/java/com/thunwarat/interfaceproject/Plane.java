/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.interfaceproject;

/**
 *
 * @author ACER
 */
public class Plane extends Vehicle implements Fly, Run {

    public Plane() {
        super("Plane");
    }

    @Override
    public void startPower() {
        System.out.println("Plane: Start Power.");
    }

    @Override
    public void stopPower() {
        System.out.println("Plane: Stop Power.");
    }

    @Override
    public void fly() {
        System.out.println("Plane: Fly.");
    }

    @Override
    public void run() {
        System.out.println("Plane: Run.");
    }
}
